console.log("Hello world!");

const { app, BrowserWindow } = require('electron');

app.on('ready', () =>{
    let mainWindow = new BrowserWindow({
        frame:false,
        toolbar: false,
        transparent: true,
        
    });
    mainWindow.loadURL(`file://${__dirname}/inicio.html`);
    mainWindow.show();
});